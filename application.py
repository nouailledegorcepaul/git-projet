#! /usr/bin/env python3
from db import *
from flask import Flask, render_template
app = Flask(__name__)
app.debug = True
global users
users = {}
@app.route('/')
def home():
    return render_template(
        "home.html")

@app.route('/user/')
@app.route('/user/<name>')
def user(name=None):
    db = DB()
    res = db.get(name)
    todos=[]
    for t in res:
        todos.append(t[1])
    return render_template(
        "user.html",
        name=name,
        todo=todos)

if __name__ == '__main__':
    app.run(host="0.0.0.0")
